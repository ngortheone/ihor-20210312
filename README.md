# Emoji Search

Created with *create-react-app*. See the [full create-react-app
guide](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Pre-requisites

- docker (build)
- web browser (run)
- make (build, optional)
- kubectl (run)
- minikube (run, or remote kubernetes cluster)
- tilt (run)

## Build

Before you begin make sure you are connected to the right docker daemon.
If you are using minikube it is much easier to build Docker containers on 
the minikube vm, using minikube's docker, rather than figuring out later
how to push your container there.

    minikube start
    eval $(minikube docker-env)

To build the app run:

    make build

or if you don't have make:

	docker build \
      --build-arg GIT_HASH=$(git rev-parse HEAD) \
      --tag emoji_search:latest \
      .

## Run


### Docker locally 

To run the application container:

    make run

or if you don't have make:

	docker run -p 8080:80 emoji_search:latest

Then open go to [http://localhost:8080](http://localhost:8080)


### Minikube

Make sure you've built the container on minikube instance.

Start minikube (if not running)

    minikube start

Point docker cli to the Docker daemon inside minikube

    eval $(minikube docker-env)

Deploy the Stateful Set and Service

    kubectl apply -f manifests/app.yaml

Make sure you have minikube tunnel running in a separate terminal. This is 
needed to get access to LoadBalancer IPs of the service. 
List services in the cluster. The output will look like this.


    $ kubectl get svc
    NAME                   TYPE           CLUSTER-IP       EXTERNAL-IP      PORT(S)          AGE
    emoji-search-backup    LoadBalancer   10.109.38.57     10.109.38.57     8080:32493/TCP   7s
    emoji-search-primary   LoadBalancer   10.106.139.159   10.106.139.159   8080:30700/TCP   7s


Open external IP of the the service in the broweser to get access to the service.
E.g.  http://10.109.38.57:8080

