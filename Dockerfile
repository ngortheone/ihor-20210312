# vim: set ft=Dockerfile :
###############################################################################
# PRELUDE
###############################################################################
# - There will be a lot of comments.
#
# - Also you asked for explanations and attribution, so here it comes.
#
# - I am that kind of a guy who obsesses over code readability and
#   maintainability, I like when every thing has its own place, each place has 
#   a well defined location and purpose. 
#
# - I also don't like long strings. 80 characters is the perfect maximum
#   width of (almost) any line.
#
# - I also enjoy what I do. That means that sometimes I leave easter eggs,
#   funny comments, rants about some particular badness for other developers to
#   discover and hopefully appreciate and enjoy.
#
# - I am using alpine linux images due to their small size and better security.
#   Better security in this case is achieved by reduced attack surface of the
#   image file system, also musl libc is more secure than glibc, but this is
#   debatable.
#
# - I am using nginx for serving static content. It is:
#   * smaller and faster than any nodejs solution
#   * has a good security track record
#   * widely adopted and understood
#
# - I don't have much experience with nodejs so I might have missed some of the
#   nuances of build/deploy procedure that some bearded nodejs developers might 
#   know. (after all nodejs was never in my resume)
#
###############################################################################
# BUILDER
###############################################################################

# Using LTS version of the nodejs because in my experience latest version can 
# cause issues or incompatibilities with various dependencies. 
# I decided to specify additionally alpine version as a constraint to avoid
# picking up a newer major version automatically, as this might cause breakage.
# https://hub.docker.com/_/node/
# https://docs.docker.com/engine/reference/builder/#from
FROM node:lts-alpine3.13 as builder

# Tilt is too opinionated and removes .git directory from docker context
# https://github.com/tilt-dev/tilt.build/blob/master/docs/file_changes.md
# https://github.com/tilt-dev/tilt/issues/2169
# This forces us to use the build time argument to pass it the hash.
# My previous solution was to just to run git command inside container
# https://docs.docker.com/engine/reference/builder/#arg
ARG GIT_HASH

# create /app directory inside of th build container
# https://docs.docker.com/engine/reference/builder/#run
RUN mkdir /app

# Make all subsequent Dockerfile commands run from this directory.
# This command is optional, but it allows to clean up the code a bit by
# shortening the paths later in the Dockerfile
# https://docs.docker.com/engine/reference/builder/#workdir
WORKDIR /app

# Copy application source code to container, avoiding things specified in 
# dockerignore file.
# Another possibility to do this is to use ADD command, but that is less secure
# as ADD allows an argument to be an arbitrary URL.
# https://docs.docker.com/engine/reference/builder/#add
# https://docs.docker.com/engine/reference/builder/#copy
COPY . .

#------------------------------------------------------------------------------
# We do not worry about glueing together all runs with && since we are using
# multistaged build and layers af this container do not bother us much.
#------------------------------------------------------------------------------

# Change the app title to current git hash
RUN  ./release.sh $GIT_HASH

# Install dependencies (e.g. reactjs)
RUN npm install

# Run tests. By default react runs tests in watch mode that leads to tests
# waiting indefinitely. To avoid this --watchAll=false is added
RUN npm run test -- --watchAll=false

# Build static production version of the application. 
# The resulting /app/build directory is our build artifact
RUN npm run build

###############################################################################
# RUNNER
###############################################################################

# https://docs.docker.com/engine/reference/builder/#from
# https://hub.docker.com/_/nginx
FROM nginx:1.18.0-alpine

# yes, this is me
# https://docs.docker.com/engine/reference/builder/#label
LABEL maintainer="Ihor Antonov <ihor@antonovs.family>"

# Copy build artifacts into nginx's html directory
# Another possibility to do this is to use ADD command, but that is less secure
# as ADD allows an argument to be an arbitrary URL.
# https://docs.docker.com/engine/reference/builder/#add
# https://docs.docker.com/engine/reference/builder/#copy
COPY --from=builder /app/build /usr/share/nginx/html

# Tell Docker that the container listens on the specified port at runtime. 
# https://docs.docker.com/engine/reference/builder/#expose
EXPOSE 8080

###############################################################################
# PROFIT
###############################################################################
# - No need to have heavy nodejs dependencies in production container
# - Nginx serves the content as non-root user (ningx)
# - Entrypoint is specified by nginx container, no need to override it

