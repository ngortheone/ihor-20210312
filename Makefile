PORT=8080
APP_NAME=ihor-20210312/emoji-search
APP_TAG=latest

PHONY: build run


build:
	docker build \
		--build-arg GIT_HASH=$(shell git rev-parse HEAD) \
		--tag $(APP_NAME):$(APP_TAG) \
		.

run:
	docker run -p $(PORT):80 $(APP_NAME):$(APP_TAG)

.DEFAULT_GOAL := build

