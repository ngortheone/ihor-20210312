#!/bin/sh
# vim: set ft=sh :
###############################################################################
# release.sh
# 
# USAGE: release.sh                  Try to determine the hash automatically 
#        release.sh aaabbbbccccdddd  Use esternally provded hash
#
# Two modes of operation are supported. Det
# This script is used to change the App Title in src/Header.js to current git
# commit hash. This is a bit tricky to get right since this script does 
# a lot of assumptions about the contents of src/Header.js file.
#
# /bin/sh is the smallest common denominator. 
# Some systems might not have bash installed, so I avoid using bash-isms here.
#
# -----------------------------------------------------------------------------
# As we all know parsing HTML with regex is bad enough so that:
#
# > if you parse HTML with regex you are giving in to Them and their
# > blasphemous ways which doom us all to inhuman toil for the One whose Name
# > cannot be expressed in the Basic Multilingual Plane, he comes. 
# 
# StackOverflow wisdom 
# https://stackoverflow.com/a/1732454
#
# I dare not to think about what will happen to the fabric of reality if we try
# to parse JSX with awk or sed :) A trivial improvement to to the code might
# save us all from impeding doom - use a separate file (e.g. VERSION) that
# is going to be embedded during build time into Header.js file.
# This way any parsing can be avoided since we can simply overwrite the VERSION
# file.
# 
# But you've asked for it and you've been warned :)
###############################################################################
# fail fast and loud in case of any error
set -o errexit
set -o nounset

str_to_replace="Emoji Search"

# Step 1 - get current git commit
# We either need to have a git directory and git installed or hash should be
# provided as an argument
hash="$(git rev-parse HEAD)" || hash="${1:?Git not found and hash not provided externally}" 

echo "Using hash $hash"

# Step 2 - replace "Emoji Search" string in Header.js file
sed -i'' "s/${str_to_replace}/${hash}/" src/Header.js

# Step 3 - hope that world still exists
